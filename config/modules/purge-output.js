// GLOBALS
var fse = require('fs-extra'),
	path = require('path'),
	projectRoot,
	imgOutPath,
	jsOutPath,
	cssOutPath;



// PROCEDURES
init();



// FUNCTIONS
function init() {
	// get project root 2 dirs above
	projectRoot = __dirname;
	projectRoot = path.dirname(projectRoot);
	projectRoot = path.dirname(projectRoot);

	imgOutPath = path.join(projectRoot, 'www', 'images');
	cssOutPath = path.join(projectRoot, 'www', 'css');
	jsOutPath = path.join(projectRoot, 'www', 'scripts');

	console.log('Purging images...');
	fse.emptyDirSync(imgOutPath);

	console.log('Purging css...');
	fse.emptyDirSync(cssOutPath);

	console.log('Purging js...');
	fse.emptyDirSync(jsOutPath);
}