//Loading Modules
var path = require('path'),
	find = require('find'),
	fs = require('fs'),
	fse = require('fs-extra'),
	UglifyJS = require('uglify-js'),
	chokidar = require('chokidar'),
	cb = require('./cache-bust');


//Global Variables
var projectRootPath,
	jsMainSrcPath,
	jsHeadSrcPath,
	jsOutputName,
	jsOutputPath,
	jsMainOutPath,
	willWatch,
	willCachebust;


// Begin
init(process.argv.splice(2));



// args, array of arguments:
function init(args) {
	// the string 'true' will enable watching
	willWatch = args[0];

	// the string 'true' will enable cachebusting
	willCachebust = args[1];

	// Path dirname three times to get project root
	projectRootPath = path.dirname(process.argv[1]);
	projectRootPath = path.dirname(projectRootPath);
	projectRootPath = path.dirname(projectRootPath);
	
	// define paths
	jsHeadSrcPath = path.join(projectRootPath, 'frontend-source', 'js', 'head');
	jsMainSrcPath = path.join(projectRootPath, 'frontend-source', 'js', 'main');
	jsOutputName = 'main.min.js';
	jsOutputPath = path.join(projectRootPath, 'www', 'scripts');
	jsMainOutPath = path.join(jsOutputPath, jsOutputName);

	// concatenate and minify all js files upon initiation
	copyHeadJs();
	minifyJs();

	if (willWatch === 'true') {
		watchJs();
	}
}


function copyHeadJs() {

	// Move head scripts to output folder - these files are not going to be processed further
	try {
		fse.copySync(jsHeadSrcPath, jsOutputPath);
		console.log('Head scripts copied');
	} catch (err) {
		console.log('Head scripts copy error: ', err);
	}
}


function minifyJs() {
	var jsSourceFiles,
		uglified,
		mapSuffix;

	mapSuffix = '.map.json';

	try {
		jsSourceFiles = find.fileSync(/\.js$/, jsMainSrcPath);
	} catch (err) {
		console.log('Unable to find any .js files in ' + jsMainSrcPath);
		return;
	}

	try {
		uglified = UglifyJS.minify(jsSourceFiles, {
			outSourceMap: jsOutputName + mapSuffix,
			sourceMapIncludeSources: true
			// sourceRoot: 'file://' + jsMainSrcPath
		});
	} catch (err) {
		console.log('\x1b[31mminifyJS error:', err.message + '\n' + '    filename: ' + err.filename + '\n' + '    line: ' + err.line + ', col: ' + err.col + ', pos: ' + err.pos, '\x1b[0m');
		return;
	}

	if (!uglified) {
		return;
	}

	// create relative paths for included source 
	// by removing the jsMainSrcPath string
	uglified.map = uglified.map.replace(new RegExp(jsMainSrcPath + path.sep, 'gim'), '');

	// write main.min.js
	try {
		fs.writeFileSync(jsMainOutPath, uglified.code, 'utf8');
		console.log('js file created!');
	} catch (err) {
		console.log('js file could not be written!');
		// throw err;
	}

	// write main.min.js.map
	if (uglified.map) {
		try {
			fs.writeFileSync(jsMainOutPath + mapSuffix, uglified.map, 'utf8');
			console.log('js source-map created!');
		} catch (err) {
			console.log('js source-map could not be written!');
			// throw err;
		}
	}

	if (willCachebust === 'true') {
		cb.init();
	}
}


function watchJs() {
	var watchHeadJs,
		watchMainJs,
		watchOpts;

	watchOpts = {
		persistent: true,

		ignored: /[\/\\]\./,
		ignoreInitial: true,
		followSymlinks: false,

		usePolling: true,
		alwaysStat: false,
		depth: undefined,
		interval: 100,
		binaryInterval: 300,

		ignorePermissionErrors: false,
		atomic: true
	};

	// Watch for any changes in head js folder
	watchHeadJs = chokidar.watch(jsHeadSrcPath, watchOpts);
	watchHeadJs
		.on('add', copyHeadJs)
		.on('change', copyHeadJs);

	// Watch for any changes in main js folder
	watchMainJs = chokidar.watch(jsMainSrcPath, watchOpts);
	watchMainJs
		.on('add', minifyJs)
		.on('change', minifyJs);
}
