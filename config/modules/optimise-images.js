//Loading Modules
var chokidar = require('chokidar'),
	Imagemin = require('imagemin'),
	find = require('find'),
	path = require('path'),
	cb = require('./cache-bust');

//Global Variables
var imagemin,
	projectRootPath,
	imgSrcPath,
	imgOutPath,
	willWatch,
	willCachebust;



// Begin
init(process.argv.splice(2));



function compressImages(filePath) {
	imagemin = new Imagemin()
		.dest(imgOutPath)
		.src(filePath);
		// .run();

	if (willCachebust === 'true') {
		imagemin.run(function (err, files) {
			cb.init();
		});
	} else {
		imagemin.run();
	}
}

// find all the images in source folder and rebuild them upon initiation
function buildAllImages() {
	var i,
		imgs = find.fileSync(/\.png|\.jpg|\.jpeg|\.svg|\.gif$/, imgSrcPath);
	
	for (i = 0; i < imgs.length; i += 1) {
		compressImages(imgs[i]);
	}
}

// args, array of arguments
// args[0]: the string 'true' will enable watching
// args[1]: the string 'true' will enable cachebusting
function init(args) {
	willWatch = args[0];
	willCachebust = args[1];

	// Path dirname three times to get project root
	projectRootPath = path.dirname(process.argv[1]);
	projectRootPath = path.dirname(projectRootPath);
	projectRootPath = path.dirname(projectRootPath);

	imgSrcPath = path.join(projectRootPath, 'frontend-source', 'graphics', 'watched');
	imgOutPath = path.join(projectRootPath, 'www', 'images');

	buildAllImages();

	if (willWatch === 'true') {
		watchImages();
	}
}


function watchImages() {
	var watcher;

	//Watch for any changes in img folder
	watcher = chokidar.watch(imgSrcPath, {
		persistent: true,

		ignored: /[\/\\]\./,
		ignoreInitial: true,
		followSymlinks: false,
		// cwd: '.',

		usePolling: true,
		alwaysStat: false,
		depth: undefined,
		interval: 100,
		binaryInterval: 300,

		ignorePermissionErrors: false,
		atomic: true
	});

	watcher
		.on('add', compressImages)
		.on('change', compressImages);
}
