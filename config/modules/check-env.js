// check that the project has local installs of certain packages
// enforces consistency of output between workstations that might
// have their own globally installed versions

var fs = require('fs'),
	path = require('path'),
	packageNames = [
		'imagemin',
		'md5-file',
		'svg-sprite',
		'svg2png',
		'uglify-js'
	];


init();

function init() {
	var isValid,
		i,
		len,
		thisPath;

	isValid = true;

	len = packageNames.length;
	for (i = 0; i < len; i += 1) {
		thisPath = path.join('node_modules', packageNames[i]);
		if (!testFolder(thisPath)) {
			isValid = false;
		}
	}

	if (isValid === true) {
		process.exit(0);
	} else {
		console.log('****************************************');
		console.log('****************************************');
		console.log('\x1b[31mEssential local packages not installed!');
		console.log('Please run `npm install`\x1b[0m');
		console.log('****************************************');
		console.log('****************************************');
		process.exit(1);
	}
}


function testFolder(path) {
	var stats;
	try {
		stats = fs.lstatSync(path);
		if (!stats.isDirectory()) {
			return false;
		}
	}
	catch (e) {
		return false;
	}
	return true;
}