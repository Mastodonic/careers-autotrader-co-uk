// https://github.com/jkphl/svg-sprite
// Configuration kickstarter - http://jkphl.github.io/svg-sprite/#json
// https://github.com/domenic/svg2png


// GLOBALS

var SVGSpriter    = require('svg-sprite'),
    svg2png       = require('svg2png'),
    path          = require('path'),
    mkdirp        = require('mkdirp'),
    fs            = require('fs'),
    fse           = require('fs-extra'),
    watchr        = require('watchr'),
    cb            = require('./cache-bust'),
    spriteName    = 'bg-sprite',
    willCachebust;


// PROCEDURES

// init(process.argv.splice(2));


// FUNCTIONS

// args, array of arguments:
function init(args) {
    // the string 'true' will enable watching
    var willWatch = args[0],
        directory = path.join('frontend-source', 'graphics', 'sprite');

    willCachebust = args[1];

    processDirectory(directory);

    if (willWatch === 'true') {
        watchSprites(directory);
    }
}


function watchSprites(directory) {
    watchr.watch({
        path: directory,
        ignoreHiddenFiles: true,
        persistent: false,
        listeners: {
            error: function (err) {
                console.log('an error occured:', err);
                return;
            },
            watching: function (err, watcherInstance) {
                if (err) {
                    console.log('watching the path ' + watcherInstance.path + ' failed with error', err);
                    return;
                } else {
                    console.log('watching the path ' + watcherInstance.path + ' completed');
                }
            },
            change: function (changeType, filePath, fileCurrentStat, filePreviousStat) {
                if (filePath.match(/.svg$/)) {
                    processDirectory(directory);
                }
            }
        }
    });
}


function processDirectory(dir) {
    var config,
        spriter,
        files,
        file,
        i;

    files = fs.readdirSync(dir);
    if (!(files && files.length && files.length > 0)) {
        return;
    }

    console.log(path.resolve(path.join(__dirname, '..', 'scss-templates', 'sprite.scss')));

    config = {
        'dest': '',
        'shape': {
            'id': {
                // Separator for directory name traversal
                'separator': '-'
            },
            'spacing': {
                // Padding around all shapes
                'padding': 5
            }
        },
        'mode': {
            // Create a css sprite
            'css': {
                // Main output directory
                'dest': path.join('www', 'css'),
                // Sprite path and name
                'sprite': path.join('..', 'images', spriteName + '.svg'),
                // Cache busting
                'bust': false,
                // Prefix for CSS selectors
                'prefix': '.icon-',
                'mixin': spriteName,
                'dimensions': true,
                'render': {
                    // Render a Sass stylesheet
                    'scss': {
                        'template': path.join(__dirname, '..', 'scss-templates', 'sprite.scss'),
                        // 'template': '/usr/local/lib/node_modules/svg-sprite/tmpl/css/sprite.scss',
                        // Sass path and name
                        'dest': path.join('..', '..', 'frontend-source', 'scss', '_sprite.scss')
                    }
                }
            }
        }
    };

    spriter = new SVGSpriter(config);

    for (i = 0 ; i < files.length; i += 1) {
        if (!files[i].toString().match(/.svg$/)) {
            continue;
        }
        file = path.join('frontend-source', 'graphics', 'sprite', files[i]);
        file = path.resolve(file);
        spriter.add(file, files[i].toString(), fs.readFileSync(file, {encoding: 'utf-8'}));
    }

    // Compile the sprite
    spriter.compile(compileSprite);
}


function compileSprite(error, result, cssData) {
    var mode,
        type,
        svgPath;

    // don't need .hasOwnProperty for keys of this object
    /* jshint forin: false */
    // Run through all configured output modes
    for (mode in result) {
        // Run through all created resources and write them to disk
        for (type in result[mode]) {
            mkdirp.sync(path.dirname(result[mode][type].path));
            fs.writeFileSync(result[mode][type].path, result[mode][type].contents);
        }
    }

    svgPath = cssData.css.sprite.split(path.sep).splice(1).join(path.sep);
    writeFallback(svgPath);
}


function writeFallback(svgPath) {
    var pngPath;

    // Get svg webpath
    // trim the first part of the path from the svg path
    pngPath = path.join('www', 'images', spriteName + '.png');

    // Create png from svg: https://github.com/domenic/svg2png
    svg2png(path.join('www', svgPath), pngPath, function (err) {
        if (err) {
            // console.log('writeFallback svg2png error:', err);
        } else {
            // copy to frontend-source: if img:watch is running it'll optimise that then copy over the png we've just made in www
            fse.copySync(pngPath, path.join('frontend-source', 'graphics', 'watched', spriteName + '.png'));
        }
        if (willCachebust === 'true') {
            cb.init();
        }
    });

}
