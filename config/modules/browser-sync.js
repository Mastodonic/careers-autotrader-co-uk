//Loading Modules
var fs = require('fs'),
	path = require('path'),
	bs = require('browser-sync').create();

var	configFileFolder,
	configFilePath,
	configFile,
	bsConfig,
	files;

files = ['www/**/*.html',
		'www/**/*.cshtml',
		'www/**/*.aspx',
		'www/scripts/main.min.js',
		'www/css/*.css',
		'www/images/*.svg',
		'www/images/*.png',
		'www/images/*.gif',
		'www/images/*.jpg'];

//Parsing the config file 
configFileFolder = path.dirname(process.argv[1]);
configFileFolder = path.dirname(configFileFolder);
configFilePath = path.join(configFileFolder, 'config.json');
configFile = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));

bsConfig = {
	proxy: configFile.domain
};

bs.watch(files).on('change', bs.reload);

bs.init(bsConfig);