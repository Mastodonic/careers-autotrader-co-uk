// GLOBALS

var path = require('path'),
    md5File = require('md5-file'),
    fs = require('fs'),
    // walk = require('walk'),
    walk = require('walkdir'),
    projectRootPath,
    wwwRoot,
    markupSrcPath,
    jsSrcPath,
    scssSrcPath,
    imgOutPath,
    jsOutPath,
    cssOutPath,
    identifier,
    imgRegEx = new RegExp('^.*\\.gif$|^.*\\.png$|^.*\\.jpg$|^.*\\.jpeg$|^.*\\.svg$', 'gim'),
    markupRegEx = new RegExp('^.*\\.html$|^.*\\.cshtml$', 'gim'),
    scssRegEx = new RegExp('^.*\\.scss$', 'gim'),
    cssRegEx = new RegExp('^.*\\.css$', 'gim'),
    jsRegEx = new RegExp('^.*\\.js$', 'gim');



// PROCEDURES

// init();
// exports.init = init;


// FUNCTIONS

function init() {
    var wwwFiles,
        assetPaths,
        cachebustedPaths;
        
    // Project root is 2 directories above
    projectRootPath = __dirname;
    projectRootPath = projectRootPath.split(path.sep);
    projectRootPath = projectRootPath.splice(0, projectRootPath.length - 2).join(path.sep);

    identifier = '__v_';

    // set folder paths
    jsSrcPath         = path.join(projectRootPath, 'frontend-source', 'js');
    scssSrcPath       = path.join(projectRootPath, 'frontend-source', 'scss');
    wwwRoot           = path.join(projectRootPath, 'www');
    markupSrcPath     = path.join(projectRootPath, 'www');
    cssOutPath        = path.join(wwwRoot, 'css');
    imgOutPath        = path.join(wwwRoot, 'images');
    jsOutPath         = path.join(wwwRoot, 'scripts');


    // 1a. cachebust images first
    // 1b. replace filenames within css, js and markup files
    wwwFiles = null;
    cachebustedPaths = null;
    assetPaths = null;
    wwwFiles = walk.sync(imgOutPath);
    // filter out non-image files
    assetPaths = wwwFiles.join('\n').match(imgRegEx);
    cachebustedPaths = cachebustFiles(assetPaths);
    replaceInFiles(cachebustedPaths, true, true, true);


    // 2a. cachebust css next
    // 2b. replace filenames within js and markup files
    wwwFiles = null;
    cachebustedPaths = null;
    assetPaths = null;
    wwwFiles = walk.sync(cssOutPath);
    // filter out non-css files
    assetPaths = wwwFiles.join('\n').match(cssRegEx);
    cachebustedPaths = cachebustFiles(assetPaths);
    replaceInFiles(cachebustedPaths, true, true, false);


    // 3. now cachebust js
    // 3b. replace filenames within markup files only
    wwwFiles = null;
    cachebustedPaths = null;
    assetPaths = null;
    wwwFiles = walk.sync(jsOutPath);
    // filter out non-js files
    assetPaths = wwwFiles.join('\n').match(jsRegEx);
    cachebustedPaths = cachebustFiles(assetPaths);
    replaceInFiles(cachebustedPaths, true, false, false);

}



function replaceInFiles(changedFiles, willChangeMarkup, willChangeJs, willChangeSass) {
    var theseFiles,
        numChangedFiles;

    // abort if any arg is not an array, is undefined or is 0 length
    if (!changedFiles || !changedFiles.length) {
        return;
    }
    numChangedFiles = changedFiles.length;

    // console.log('replaceInFiles:', changedFiles);

    if (willChangeMarkup === true) {
        theseFiles = walk.sync(markupSrcPath);
        theseFiles = theseFiles.join('\n').match(markupRegEx);
        replaceInFilesLoop(theseFiles, changedFiles);
    }

    if (willChangeJs === true) {
        theseFiles = walk.sync(jsSrcPath);
        theseFiles = theseFiles.join('\n').match(jsRegEx);
        // console.log('js paths: ', theseFiles);
        replaceInFilesLoop(theseFiles, changedFiles);
    }
    
    if (willChangeSass === true) {
        theseFiles = walk.sync(scssSrcPath);
        theseFiles = theseFiles.join('\n').match(scssRegEx);
        // console.log('scss paths: ', theseFiles);
        replaceInFilesLoop(theseFiles, changedFiles);
    }
}


function replaceInFilesLoop(targetFiles, changedFiles) {
    var numFiles,
        numChangedFiles,
        thisFile,
        origData,
        thisData,
        i,
        j;

    if (!targetFiles) {
        return;
    }
    
    numChangedFiles = changedFiles.length;
    numFiles = targetFiles.length;
    for (i = 0; i < numFiles; i += 1) {
        thisFile = targetFiles[i];
        origData = fs.readFileSync(thisFile, {encoding: 'utf8'});
        thisData = fs.readFileSync(thisFile, {encoding: 'utf8'});
        for (j = 0; j < numChangedFiles; j += 1) {
            thisData = cachebustFilenameInString(changedFiles[j], thisData);
        }
        // console.log(thisData);
        if (thisData !== origData) {
            // write file back
            console.log('Writing source file after cachebusting files it references: ' + thisFile);
            fs.writeFileSync(thisFile, thisData);
        }
    }
}


function cachebustFilenameInString(cacheBustedFilePath, strData) {
    var unbustedFilename,
        thisExt,
        bustedFilename,
        cachebustRegEx;

    
    // console.log('cachebustFilenameInString:', cacheBustedFilePath);

    // find existing cachebusted references to this file
    thisExt = path.extname(cacheBustedFilePath);
    bustedFilename = path.basename(cacheBustedFilePath, thisExt);
    unbustedFilename = bustedFilename.replace(new RegExp(identifier + '[A-Za-z0-9]{6}', 'g'), '');
    // search for name with or without cachebusted string
    cachebustRegEx = new RegExp(unbustedFilename + '(?:' + identifier + '[A-Za-z0-9]{6})?' + thisExt, 'gim');

    strData = strData.replace(cachebustRegEx, bustedFilename + thisExt);

    return strData;
}


function cachebustFile(thisPath) {
    var hash,
        basePath,
        ext,
        fileName,
        newPath;

    // console.log('cachebustFile:', thisPath);
    if (thisPath.match(identifier) !== null) {
        // if this path contains the identifier skip and go to the next one
        return null;
    }
    hash = md5File(thisPath);
    hash = hash.substr(hash.length - 6);
    basePath = path.dirname(thisPath);
    ext = path.extname(thisPath);
    fileName = path.basename(thisPath, ext);
    fileName = fileName.replace(/[^-A-Za-z0-9_@.]/g, '_');
    fileName = fileName + identifier + hash + ext;
    newPath = basePath + path.sep + fileName;
    fs.renameSync(thisPath, newPath);
    return newPath;
}

function removeOldCachebustedFiles(unCachebustedPath, cachebustedPath) {
    var thisFolder,
        regex,
        foundFiles,
        len,
        i,
        thisFilePath,
        ext,
        origFileName,
        thisFileName;

    thisFolder = path.dirname(unCachebustedPath);
    foundFiles = fs.readdirSync(thisFolder);

    // console.log('removeOldCachebustedFiles starts:', unCachebustedPath, cachebustedPath);

    len = foundFiles.length;
    for (i = 0; i < len; i += 1) {
        thisFilePath = path.join(thisFolder, foundFiles[i]);
        ext = path.extname(thisFilePath);
        thisFileName = path.basename(thisFilePath, ext);
        origFileName = path.basename(unCachebustedPath, ext);
        regex = new RegExp(origFileName + identifier + '[A-Za-z0-9]{6}', 'g');
        // console.log('removeOldCachebustedFiles: regex', regex);
        if (thisFileName.match(regex) && thisFilePath !== cachebustedPath) {
            // console.log('Will delete old cachebusted file: ' + thisFilePath);
            fs.unlinkSync(thisFilePath);
            console.log('Deleted old cachebusted file: ' + thisFilePath);
        }
    }
}


function cachebustFiles(origPaths) {
    var i,
        numFiles,
        newPath,
        renamedFiles;

    // console.log(origPaths);
    if (!origPaths) {
        return;
    }
    renamedFiles = [];
    numFiles = origPaths.length;
    for (i = 0; i < numFiles; i += 1) {
        newPath = cachebustFile(origPaths[i]);
        if (newPath !== null) {
            removeOldCachebustedFiles(origPaths[i], newPath);
            renamedFiles.push(newPath);
        }
    }

    return renamedFiles;
}

/*
function cachebustFolders(dirPaths, extensions) {
    var numDirs,
        i,
        thisPath,
        thisStat,
        j,
        extRegEx,
        files,
        numFiles,
        pathsToCachebust;

    numDirs = dirPaths.length;

    // prep regex string
    extensions = extensions.join('$|\\.');
    extensions = '\\.' + extensions + '$';
    extRegEx = new RegExp(extensions);

    pathsToCachebust = [];
    for (i = 0; i < numDirs; i += 1) {
        // ensure path is a folder
        thisPath = dirPaths[i];
        try {
            // fs.openSync(thisPath);
            thisStat = fs.statSync(thisPath);
            if (thisStat.isDirectory === false) {
                console.log('cachebustFolders: ' + thisPath + ' is not a directory.');
                continue;
            }
        } catch (err) {
            continue;
        }
        // console.log('cachebustFolders: ' + thisPath + ' is a directory.');

        files = fs.readdirSync(thisPath);
        numFiles = files.length;
        for (j = 0; j < numFiles; j += 1) {
            if (files[j].match(extRegEx) !== null) {
                pathsToCachebust.push(thisPath + path.sep + files[j]);
            }
        }
    }
    return pathsToCachebust;
}
*/