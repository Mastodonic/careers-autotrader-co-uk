// Load Modules
var watchr = require('watchr'),
	execSync = require('child_process').execSync,
	fs = require('fs'),
	path = require('path'),
	cb = require('./cache-bust');


//Global Variables
var willWatch,
	willCachebust,
	projectRootPath,
	scssPath,
	cssPath;


// Begin
init(process.argv.splice(2));


// args, array of arguments
// args[0]: the string 'true' will enable watching
// args[1]: the string 'true' will enable cachebusting
function init(args) {
	willWatch = args[0];
	willCachebust = args[1];

	// Path dirname three times to get project root
	projectRootPath = path.dirname(process.argv[1]);
	projectRootPath = path.dirname(projectRootPath);
	projectRootPath = path.dirname(projectRootPath);

	scssPath = path.join(projectRootPath, 'frontend-source', 'scss');
	cssPath = path.join(projectRootPath, 'www', 'css');

	buildCSS();

	if (willWatch === 'true') {
		watchCSS();
	}
}

function buildCSS() {
	var execString;

	execString = 'sass --scss --style compressed --sourcemap=inline ' + scssPath + ':' + cssPath + ' --update';

	try {
		execSync(execString, {stdio: 'ignore'});
	} catch (err) {
		console.log('\x1b[31mbuildCSS error:', err.toString(), '\x1b[0m');
	}

	if (willCachebust === 'true') {
		cb.init();
	}
}

function watchCSS() {
	watchr.watch({
		paths: scssPath,
		ignoreHiddenFiles: true,
		persistent: false,
		listeners: {
			error: function (err) {
				console.log('an error occured:', err);
				return;
			},
			watching: function (err, watcherInstance) {
				if (err) {
					console.log('watching path ' + watcherInstance.path + ' failed with error', err);
					return;
				} else {
					console.log('watching path ' + watcherInstance.path);
				}
			},
			change: function (changeType, filePath, fileCurrentStat, filePreviousStat) {
				if (changeType === 'update' || changeType === 'create') {
					console.log(filePath, ' ', changeType);
					buildCSS(filePath);
				} else if (changeType === 'delete') {
				}
			}
		}
	});
}