var APP = APP || {};

APP.toggleContent = APP.toggleContent || (function () {
	'use strict';
	var $contentDiv,
		$toggleBtn,
		$toggleBtnWrap,
		isCollapsed,
		msgToClose,
		msgToOpen,
		openedClassname;

	function init() {
		isCollapsed = true;
		msgToOpen = 'Read more';
		msgToClose = 'Read less';
		openedClassname = 'open';
		$contentDiv = $('.long-content-wrap');
		$toggleBtnWrap = $('.long-content-toggle');
		$toggleBtn = $('.long-content-toggle-btn');
		$toggleBtn.text(msgToOpen);
		$toggleBtn.on('click', toggle);
	}

	function toggle() {
		if (isCollapsed === true) {
			showContent();
			isCollapsed = false;
		} else {
			hideContent();
			isCollapsed = true;
		}
	}

	function showContent() {
		$contentDiv.addClass(openedClassname);
		$toggleBtnWrap.addClass(openedClassname);
		$toggleBtn.text(msgToClose);
	}

	function hideContent() {
		$contentDiv.removeClass(openedClassname);
		$toggleBtnWrap.removeClass(openedClassname);
		$toggleBtn.text(msgToOpen);
		$('html, body').animate({ scrollTop: $contentDiv.offset().top }, 500);
	}

	return {
		init: init
	};

}());