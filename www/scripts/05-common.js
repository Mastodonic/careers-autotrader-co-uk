var APP = APP || {};

APP.common = APP.common || (function () {
	'use strict';
	var $window = $(window),
		ww = $window.width(),
		wh = $window.height();
		
	function fullHeight(selector, offset) {
		$(selector).height(wh - offset);
		$window.resize(function () {
			$(selector).height($window.height() - offset);
		});
	}

	function heightResize(selector, targetSelector) {
		$(selector).height(targetSelector(selector));
	}

	function smoothScrolling(speed, offset) {
		$('a[href^=#]').on('click', function (e) {
			// don't smooth scroll on mobile
			// (css that makes nav work breaks smooth scroll)
			if ($('#navigation-toggle').is(':visible')) {
				return;
			}
			e.preventDefault();
			var destination = $(this).attr('href'),
				distance = $(destination).offset().top - offset;
			$('html, body').animate({ scrollTop: distance }, speed);
		});
	}

	function lightbox() {
		$('.lightbox-trigger').click(function () {
			var $body = $('body'),
				target = $(this).attr('data-target'),
				$target = $(target),
				$overlay = $target.find('.lightbox-overlay'),
				$canvas = $target.find('.lightbox-canvas'),
				$content = $target.find('.lightbox-content');
			
			//Create an overlay if it doesn't exist already
			if (!$overlay.length) {
				$canvas.append('<div class="lightbox-overlay"></div>');
				$content.append('<div class="close-button"></div>');
				$overlay = $(target).find('.lightbox-overlay');
			}

			//add a class open 
			$target.addClass('open');
			$body.addClass('lightbox-open');

			//close the lightbox when cicking on the overlay
			$('.lightbox-overlay, .close-button').click(function () {
				$target.removeClass('open');
				$body.removeClass('lightbox-open');
			});
		});
	}

	function contactTab() {
		var $anchor = $('.tab-nav button'),
			$tabContainer = $('.tab-content'),
			$tabNav = $('.tab-nav li'),
			$tabs = $('.tab-content .tab'),
			$popup = $('#infobox'),
			$target,
			$targetItem;

		$anchor.on('click touchstart', function () {
			$target = $(this).attr('data-target');
			$targetItem = $tabContainer.find('#' + $target);
			$popup.removeClass('open');
			$tabNav.removeClass('selected');
			$(this).parent().addClass('selected');
			$tabs.removeClass('selected');
			$targetItem.addClass('selected');
		});
	}

	function clearSearchForm() {
		$('.reset input').click(function(){
			var activeItem = $('.m-job-search').find('li.active'),
				locationLabel = 'Location',
				divisionLabel = 'Division';

			activeItem.removeClass('active');
			$('.filter-canvas .location .selected').html(locationLabel);
			$('.filter-canvas .division .selected').html(divisionLabel);
		})
	}

	function formValidation() {
		//Form Validation
		if ($('body').hasClass('single-post')) {
			$('textarea').on('blur', function(){
				var alert;
				if ($(this).val() === '') {
					alert = $(this).parents('.item').find('.alert');
					console.log(alert);
					if (!alert.length) {
						$(this).parents('.item').addClass('bad')
												.append('<div class="alert">Please fill the required field</div>');
					}
				} else {
					$(this).parents('.item').removeClass('bad');
					alert = $(this).parents('.item').find('.alert');
					alert.remove();
				}
			});

			validator.message.empty = 'Please fill the required field';
			$('form').on('blur', 'input[required]', validator.checkField);


			$('form').submit(function (e) {
				e.preventDefault();
				var submit = true;

				// check all the rerquired fields
				if( !validator.checkAll( $(this) ) ) {
					submit = false;
					$(this).find('.item').addClass('bad')
				}
				if( submit ) {
					this.submit();
				}

				return false;
			})
		}
	}

	function init() {
	
		lightbox();
		contactTab();
		mapLegened();
		clearSearchForm();
		formValidation();



		if (ww > 1025) {
			stickyNav();
			smoothScrolling(500, 36);
			fullHeight('#home-carousel .slide', 141);
		} else {
			fullHeight('#home-carousel .slide', 50);
			smoothScrolling(500, 50);
		}



		if ($('html').hasClass('lt-ie9')) {
			if (ww < 1025) {
				fullHeight('.m-carousel .slide', 50);
			}
			if (ww > 1025) {
				fullHeight('.m-carousel .slide', 138);
			}			
		}
	}

	function mapLegened() {
		var $container = $('.map-legend'),
			$trigger = $('.map-legend li'),
			$description = $('.map-legend .info');
		$trigger.on('click', function () {
			var $this = $(this);

			if ($this.hasClass('open')) {
				$this.removeClass('open');
				$description.slideUp(300);
			} else {
				$description.slideUp(300);
				$trigger.removeClass('open');
				$(this).addClass('open');
				$(this).find('.info').slideDown(300);
			}
		});
	}

	function stickyNav() {
		var sticky = new Waypoint.Sticky({
			element: $('#navigation-primary')
		});
	}

	return {
		init: init
	};

}());

