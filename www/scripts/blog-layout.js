var APP = APP || {};

APP.blogLayout = (function () {
	'use strict';
	var $postListContainer = $('.m-blog-list'),
		$postList = $('#post-list'),
		$postListImgs = $('#post-list img'),
		$filterItem = $('#post-filter li');

	function init() {
		$postListContainer.css({
			'min-height': $(window).height() + 'px'
		});

		//Check if the images are loaded using imagesLoaded library before launching masonry layout
		$postList.imagesLoaded(function () {
			$postList.isotope({
				itemSelector: '.post-item'
			})
		});

		//Filter
		$filterItem.click(function(){
			$filterItem.removeClass('active');
			$(this).addClass('active');
			var filterCategory = $(this).attr('data-filter');
			$postList.isotope({ filter: filterCategory });
		});
	}
	return {
		init: init
	};
}());