var APP = APP || {};

APP.mCarousel = (function () {
	'use strict';

	var owl = $('#home-carousel'),
		position;

	//Image Preloading
	function getImgPaths(mainContainer) {

		var imgList = [],
			container = $(mainContainer),
			imgContainer = container.find('*[data-image]'),
			bgContainer = container.find('*[data-background]');

		imgContainer.each(function () {
			var img = $(this).attr('data-image');
			imgList.push(img);
		});

		bgContainer.each(function () {
			var img = $(this).attr('data-background');
			imgList.push(img);
		});
		return imgList;
	}

	function renderAssets(mainContainer, playSlider) {

		var container = $(mainContainer),
			imgContainer = container.find('*[data-image]'),
			bgContainer = container.find('*[data-background]');

		imgContainer.each(function () {
			var src = $(this).attr('data-image');
			$(this).append('<img src="' + src + '" alt="">');
		});

		bgContainer.each(function () {
			var bgsrc = $(this).attr('data-background');
			$(this).css({
				'background-image': 'url(' + bgsrc + ')'
			});
		});

		container.addClass('loaded').trigger('classChange');

		if (typeof(playSlider) === 'number' && $('html').hasClass('csstransforms3d')) {
			APP.animation.playSlide(playSlider);
		}

	}

	function prepSlide0() {
		renderAssets('.slide-0', 0);
		$.imgpreload(getImgPaths('.slide-1, .slide-2, .slide-3'), prepSlide123);
	}

	function prepSlide123() {
		renderAssets('.slide-1, .slide-2, .slide-3');
		$.imgpreload(getImgPaths('.slide-4, .slide-5, .slide-6'), prepSlide456);
	}

	function prepSlide456() {
		renderAssets('.slide-4, .slide-5, .slide-6');
		$.imgpreload(getImgPaths('.slide-7, .slide-8, .slide-9'), prepSlide789);
	}

	function prepSlide789() {
		renderAssets('.slide-7, .slide-8, .slide-9');
	}

	function handleCarouselInit(evt) {
		position = evt.item.index;
		//Carousel Navigation
		carouselNav('#carousel-prev', 'prev.owl.carousel');
		carouselNav('#carousel-next', 'next.owl.carousel');
		carouselNav('#start-here', 'next.owl.carousel');
	}

	

	function handleCarouselChange(evt) {
		var slideClass,
			lastItem;
		position = evt.item.index;
		slideClass = '.slide-' + position;

		if ($('html').hasClass('csstransforms3d')) {
			if (position !== null) {
				APP.animation.playSlide(position);
			} else {
				$(slideClass).on('classChange', function () {
					setTimeout(function () {
						APP.animation.playSlide(position);
					}, 800);
				});
			}
		}

		// Rewind to the first 
		lastItem = evt.item.count - 1;
		if (position === lastItem) {
			$('#carousel-next').addClass('hidden');
		} else {
			$('#carousel-next').removeClass('hidden');
		}

		// Show arrows on the second slide
		if (position === 0 || position === null) {
			$('#carousel-next, #carousel-prev').hide();
		} else {
			$('#carousel-next, #carousel-prev').show();
		}

		//Close lightbox when changing the slide
		$('body').removeClass('lightbox-open');
		$('.lightbox').removeClass('open');
	}

	//visit the site for full list of events
	//http://www.owlcarousel.owlgraphic.com/docs/api-events.html
	function carouselNav(selector, event) {
		$(selector).click(function () {
			owl.trigger(event);
		});
	}

	function goToSlide(selector, position, speed) {
		$(selector).click(function () {
			owl.trigger('to.owl.carousel', [position, speed, true]);
		});
	}

	function simpleTab(selector) {
		var $container = $(selector),
			$navItem = $container.find('.nav-item'),
			$tabContent = $container.find('.content-item');

		$navItem.click(function () {
			var $currenntTab = '.' + $(this).attr('data-tab');
			$currenntTab = $($currenntTab);

			$(this).siblings().removeClass('active');
			$(this).addClass('active');

			$(this).parent().siblings('.tab-content').children('.content-item').removeClass('active');
			$currenntTab.addClass('active');
		});

	}

	function keyboardShortcut() {
		$(document).keyup(function (i) {
			if(i.keyCode==37) {
				$('#home-carousel').trigger('prev.owl.carousel');
			} else if (i.keyCode==39) {
				$('#home-carousel').trigger('next.owl.carousel');
			}
		});
	}

	function init() {

		owl.on('initialized.owl.carousel', handleCarouselInit);
		owl.on('changed.owl.carousel', handleCarouselChange);

		keyboardShortcut();

		$.imgpreload(getImgPaths('#slide-0'), prepSlide0);

		$('#home-carousel').owlCarousel({
			autoplay: false,
			items: 1,
			loop: false,
			nav: false,
			dots: false, // Show dots navigation
			margin: 0, // margin-right(px) on item.
			navSpeed: 900, // Navigation speed
			mouseDrag: false,
			// dragEndSpeed: 500,
			smartSpeed: 500,
			autoplayHoverPause: true,
			stagePadding: 0, //Padding left and right on stage (can see neighbours)
			responsive: {
				// breakpoint from 768 up
				768: {
					stagePadding: 0
				}
			}
		});

		simpleTab('.simple-tab');

		//Image gallery within the carousel
		$('#image-gallery').lightSlider({
			gallery: true,
			item: 1,
			loop: false,
			thumbItem: 4,
			slideMargin: 0,
			enableDrag: false,
			enableTouch: false,
			animateThumb: false,
			currentPagerPosition: 'left',
			mode: 'fade'
		});

		$('#glassdoor-rating').owlCarousel({
			// autoplay: true,
			autoplayTimeout: 3000,
			autoplayHoverPause: false,
			items: 1,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			loop: true,
			nav: true,
			mouseDrag: false,
			touchDrag: false,
			navText: [
				'<span class="icon"></span>',
				'<span class="icon"></span>'
			],
			dots: false, // Show dots navigation
			margin: 0, // margin-right(px) on item.
			navSpeed: 400, // Navigation speed
			stagePadding: 0, //Padding left and right on stage (can see neighbours)
			responsive: {
				// breakpoint from 768 up
				768: {
					stagePadding: 0
				}
			}
		});
	}

	return {
		init: init
	};
}());