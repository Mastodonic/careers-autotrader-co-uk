var APP = APP || {};

APP.maps = APP.maps || (function() {
	'use strict';
	var i,
		styledMap,
		LondonMap,
		ManchesterMap,
		mapOptions,
		styles,
		marker,
		markers = [],
		$popup = $('#infobox'),
		$iconContainer = $('.map-legend'),
		$icon = $iconContainer.find('li'),
		$lMap = $('#map-london');
	//Map Styling
	styles = [{
		'featureType': 'poi.park',
		'stylers': [{
			'color': '#fafff4'
		}]
	}, {
		'featureType': 'transit.line',
		'stylers': [{
			'visibility': 'simplified'
		}, {
			'color': '#ffffff'
		}]
	}, {
		'featureType': 'poi.medical',
		'stylers': [{
			'visibility': 'simplified'
		}, {
			'color': '#eaede9'
		}]
	}, {
		'featureType': 'water',
		'stylers': [{
			'visibility': 'simplified'
		}, {
			'color': '#eff1f3'
		}]
	}, {
		'elementType': 'labels.text.fill',
		'stylers': [{
			'visibility': 'on'
		}, {
			'color': '#808080'
		}, {
			'lightness': 16
		}]
	}, {
		'featureType': 'landscape',
		'stylers': [{
			'lightness': 24
		}]
	}, {
		'featureType': 'road.highway',
		'stylers': [{
			'lightness': 32
		}, {
			'saturation': -100
		}]
	}, {
		'featureType': 'transit.station',
		'elementType': 'labels.text.fill',
		'stylers': [{
			'lightness': -42
		}]
	}, {}, {
		'featureType': 'road',
		'elementType': 'labels.text',
		'stylers': [{
			'visibility': 'off'
		}]
	}];

	function init() {
		styledMap = new google.maps.StyledMapType(styles, {
			name: 'B&W'
		});
		mapOptions = {
			scrollwheel: false,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			},
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL
			}
		};

		LondonMap = new google.maps.Map(document.getElementById('map-london'), mapOptions);
		ManchesterMap = new google.maps.Map(document.getElementById('map-manchester'), mapOptions);

		LondonMap.mapTypes.set('map_style', styledMap);
		ManchesterMap.mapTypes.set('map_style', styledMap);

		LondonMap.setMapTypeId('map_style');
		ManchesterMap.setMapTypeId('map_style');

		//Add the Markers
		constructMarker();

		//When clicking on map legend icons
		$iconContainer.on('click touchstart', 'li', function () {
			var currentId = $(this).attr('data-id');
			handleIconClick(currentId);
		});

		handleHover();
	}
	function handleIconClick(id) {
		for (i = 0; i < markers.length; i += 1) {
			if (id === markers[i].id) {
				google.maps.event.trigger(markers[i], 'click');
			}
		}
	}
	function handleMarkerClick(marker) {
		var id = marker.id,
			content,
			$currentItem,
			$name,
			$cat,
			$desc,
			$container = $('#tab-content'),
			$popupCaption = $popup.find('.label'),
			$popupContent = $popup.find('.content');

		$currentItem = $container.find('*[data-id=' + id + ']');
		$name = $currentItem.find('.name').html();
		$desc = $currentItem.find('.description').html();
		$cat = $currentItem.find('figcaption').html();
		$cat += '<div class="close"></div>';

		content = '<h3>' + $name + '</h3>';
		content += '<p>' + $desc + '</p>';

		$popup.addClass('open');

		$popupCaption.html($cat);
		$popupContent.html(content);

		handlePopupPoistion();
		handleClose();
	}

	function handlePopupPoistion() {
		var h = $lMap.height();
		$popup.css({bottom: h / 2 + 75 + 'px'});
	}

	function handleHover() {
		if (!Modernizr.touch) {
			$iconContainer.on('mouseenter', 'li', function () {
				$icon.addClass('de-active');
				$(this).removeClass('de-active');
			});
			$iconContainer.on('mouseleave', 'li', function () {
				$icon.removeClass('de-active');
			});
		}
	}

	function handleClose() {
		var $popupClose = $popup.find('.close');
		$popupClose.on('click touchstart', function () {
			$popup.removeClass('open');
		});
	}

	function constructMarker() {
		var latLang,
			londonBounds,
			manchesterBounds;
			

		//create bounds object
		londonBounds = new google.maps.LatLngBounds();
		manchesterBounds = new google.maps.LatLngBounds();

		for (i = 0; i < londonPins.length; i += 1) {
			latLang = new google.maps.LatLng(londonPins[i].lat, londonPins[i].lng);

			marker = new google.maps.Marker({
				position: latLang,
				id: londonPins[i].id,
				index: i,
				icon: londonPins[i].pin,
				map: LondonMap
			});

			markers.push(marker);
			//Extend the current Bounds
			londonBounds.extend(latLang);

			google.maps.event.addListener(marker, 'click', function () {
				handleMarkerClick(this);
				//Center map on current marker
				LondonMap.setCenter(this.getPosition());
			});
		}

		for (i = 0; i < manchesterPins.length; i += 1) {
			latLang = new google.maps.LatLng(manchesterPins[i].lat, manchesterPins[i].lng);

			marker = new google.maps.Marker({
				position: latLang,
				id: manchesterPins[i].id,
				icon: manchesterPins[i].pin,
				map: ManchesterMap
			});

			//Extend the current Bounds
			manchesterBounds.extend(latLang);

			markers.push(marker);

			google.maps.event.addListener(marker, 'click', function () {
				handleMarkerClick(this);
				//Center map on current marker
				ManchesterMap.setCenter(this.getPosition());
			});
		}
		//Center and zoom the map to the calculated position
		LondonMap.fitBounds(londonBounds);
		ManchesterMap.fitBounds(manchesterBounds);

		return markers;
	}
	return {
		init: init
	};

}());