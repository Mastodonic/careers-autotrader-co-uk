var APP = APP || {};

APP.navigation = APP.navigation || (function () {
	'use strict';
	
	var $navToggle = $('#navigation-toggle'),
		$container = $('body'),
		$mainContent = $('#main-content'),
		$navItem = $('#navigation-primary .next-level'),
		$childLevel = $('#navigation-primary .child-level'),
		$desktopSearchToggle = $('#desktop-search-toggle'),
		$mobileSearchToggle = $('#mobile-search-toggle'),
		$menuCanvas = $('#header .top'),
		$backButton = $('.active .title'),
		ww = $(window).width();

	function init() {
		$(window).resize(closeMenu);
		$(window).on('scroll', handleScroll);
		$navItem.on('click', handleMultiLevel);
		if (Modernizr.touch) {
			$navToggle.on('touchstart', toggleHandler);
			$mainContent.on('touchstart', closeMenu);
			$mobileSearchToggle.on('touchstart', handleSearchToggle);
			$desktopSearchToggle.on('touchstart', handleSearchToggle);
			$menuCanvas.on('touchstart', closeMultiLevel);
			$backButton.on('touchstart', closeMultiLevel);
			$mainContent.on('touchstart', closeSearch);
		} else {
			$navToggle.on('click', toggleHandler);
			$mainContent.on('click', closeMenu);
			$mobileSearchToggle.on('click', handleSearchToggle);
			$desktopSearchToggle.on('click', handleSearchToggle);
			$menuCanvas.on('click', closeMultiLevel);
			$backButton.on('click', closeMultiLevel);
			$mainContent.on('click', closeSearch);
		}
	}

	function handleScroll() {
		// bail out if mobile nav is visible, or drop-down search box is NOT active.
		if ($navToggle.is(':visible') === true || $container.hasClass('search-active') === false) {
			return;
		}

		// without '.stuck' the header shows the default search box so hide the drop-down search box
		if (!$('#navigation-primary').hasClass('stuck')) {
			$container.removeClass('search-active');
		}
	}

	function handleSearchToggle() {
		if (!$container.hasClass('search-active') && !$container.hasClass('menu-open')) {
			$container.addClass('search-active');
		} else {
			$container.removeClass('search-active');
		}
	}

	function closeSearch() {
		$container.removeClass('search-active');
	}
	function closeMenu() {
		$container.removeClass('menu-open second-level');
		$childLevel.removeClass('active');
	}
	function closeMultiLevel() {
		$container.removeClass('second-level');
		$childLevel.removeClass('active');
	}
	function toggleHandler() {
		$container.removeClass('search-active');
		if ($container.hasClass('menu-open')) {
			$container.removeClass('menu-open second-level');
			$childLevel.removeClass('active');
		} else {
			$container.addClass('menu-open');
		}
	}

	function handleMultiLevel() {
		var $this = $(this),
			currentSubList = $this.next('.child-level');
		
		$childLevel.removeClass('active');
		$container.removeClass('second-level');

		if (!$childLevel.hasClass('active')) {
			if (currentSubList.length) {
				currentSubList.addClass('active');
				$container.addClass('second-level');
			}
		}
	}

	return {
		init: init
	};

}());