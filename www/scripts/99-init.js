
$(document).ready(function () {
	'use strict';
	var body = $('body');

	APP.common.init();
	APP.navigation.init();
	APP.toggleContent.init();
	if ($(window).width() < 1025) {
	}
	if (body.hasClass('home')) {
		APP.mCarousel.init();
		APP.animation.init();
	}
	if (body.hasClass('blog-landing-page')) {
		APP.blogLayout.init();
	}
	if (body.hasClass('awesome-locations')) {
		APP.maps.init();
	}
});