/* Main Javascript File */
/* global TweenMax, Bounce, Waypoint, TimelineMax, console, preLoader */

var APP = APP || {};

APP.animation = (function () {
	'use strict';

	var	tmaxObject,
		slides = [];

	function init() {
		//HomePage Carousel
		if (Modernizr.csstransforms3d){
			makeSlide0();
			makeSlide1();
			makeSlide2();
			makeSlide3();
			makeSlide4();
			makeSlide5();
		}
	}

	function playSlide(slideNo) {
		var i;
		if (slideNo !== undefined) {
			for (i = 0; i < slides.length; i += 1) {
				slides[i].pause();
			}
			slides[slideNo].restart();
		}
	}

	function makeSlide0() {
		var timeline = new TimelineMax(),
			letters = headerAnimation('#slide-0 .letter', 0.7, 0.10),
			yourAdventure = scale('#your-adventure', 1, 'Elastic.easeOut'),
			startHere = scale('#start-here .cta', 0.4),
			arrowLeft = slide('#arrow-left', 1, '-100%', '0', 'Elastic.easeOut'),
			arrowRight = slide('#arrow-right', 1, '100%', '0', 'Elastic.easeOut'),
			star = starFall('#star', 2, '5%', '-200%', 'Bounce.easeOut'),
			arrowLeftWobble = wobble('#arrow-left', 1, '10%', '0', 'Elastic.easeOut'),
			arrowRightWobble = wobble('#arrow-right', 1, '-10%', '0', 'Elastic.easeOut');

		timeline.add(letters)
				.add(yourAdventure, '-=0.5')
				.add(startHere, '-=0.5')
				.add([arrowLeft, arrowRight])
				.add(star, '-=0.5')
				.add([arrowLeftWobble, arrowRightWobble])
				.pause();
		slides.push(timeline);
	}

	function makeSlide1() {
		var timeline = new TimelineMax(),
			raysRotation = rotate('#slide-1 .rays', 120, -360),
			raysFadeIn = fadeIn('#slide-1 .rays', 0.5),
			title = scale('#slide-1 .forever-evolving', 0.4),
			building = slide('#slide-1 .building-plate', 0.5, 0, '100%'),
			seeOurValues = fadeIn('#slide-1 .see-our-values .text', 0.4),
			titleHover = pulse('#slide-1 .forever-evolving .hover', 1, 'Power3.easeOut', -1),
			arrowLeft = slide('#slide-1 .arrow-left', 1, '-100%', '0', 'Elastic.easeOut'),
			arrowRight = slide('#slide-1 .arrow-right', 1, '100%', '0', 'Elastic.easeOut'),
			arrowLeftWobble = wobble('#slide-1 .arrow-left', 1, '10%', '0', 'Elastic.easeOut'),
			arrowRightWobble = wobble('#slide-1 .arrow-right', 1, '-10%', '0', 'Elastic.easeOut');

		timeline.add(raysFadeIn)
				.add(title)
				.add(building, '-=.5')
				.add(seeOurValues)
				.add([arrowLeft, arrowRight])
				.add([arrowLeftWobble, arrowRightWobble])
				.add([raysRotation, titleHover], '-=9')
				.pause();
		slides.push(timeline);
	}

	function makeSlide2() {
		var timeline = new TimelineMax(),
			bg = fadeIn('#slide-2 .slide-bg', 0.8),
			title = scale('#slide-2 .skateboard', 0.4),
			takeTheTour = scale('#slide-2 .skateboard-container .text', 0.4),
			arrowLeft = slide('#slide-2 .arrow-left', 1, '-100%', '0', 'Elastic.easeOut'),
			arrowRight = slide('#slide-2 .arrow-right', 1, '100%', '0', 'Elastic.easeOut'),
			arrowLeftWobble = wobble('#slide-2 .arrow-left', 1, '10%', '0', 'Elastic.easeOut'),
			arrowRightWobble = wobble('#slide-2 .arrow-right', 1, '-10%', '0', 'Elastic.easeOut');

		timeline.add(bg)
				.add(title)
				.add(takeTheTour, '+=.2')
				.add([arrowLeft, arrowRight])
				.add([arrowLeftWobble, arrowRightWobble])
				.pause();
		slides.push(timeline);
	}

	function makeSlide3() {
		var timeline = new TimelineMax(),
			bgFade = fadeIn('#slide-3 .slide-bg', 0.8),
			canvas = fadeIn('#slide-3 .slide-canvas', 1),
			title = scale('#slide-3 .simple-tab', 0.5, 'Power0.easeNone'),
			bgZoom = bgScaleTo('#slide-3 .slide-bg', 40, 1.4, 'Power0.easeNone', 3, true),
			astronaut = floating('#slide-3 .astronaut', 5, -50, 1, -1),
			shuttle = floating('#slide-3 .shuttle', 7, 30, 0.5, -1),
			blaster = slideTo('#slide-3 .blaster', 2, 100, 0, 'Power0.easeNone', -1, true),
			bullet = shoot('#slide-3 .bullet', 1.5, -1500, 'Power0.easeNone', 1, -1),
			arrowLeftWobble = wobble('#slide-3 .arrow-left', 1, '10%', '0', 'Elastic.easeOut'),
			arrowRightWobble = wobble('#slide-3 .arrow-right', 1, '-10%', '0', 'Elastic.easeOut');

		timeline.add([bgFade, canvas])
				.add(title, '-=0.5')
				.add([bgZoom, astronaut, shuttle, blaster, bullet], 0)
				.pause();
		slides.push(timeline);
	}

	function makeSlide4() {
		var timeline = new TimelineMax(),
			raysRotation = rotate('#slide-4 .rays', 120, -360),
			raysFadeIn = fadeIn('#slide-4 .rays', 0.5),
			vanWheelRight = rotate('#slide-4 .van .wheel.right', 0.6, -360, -1, 'Power0.easeNone'),
			vanWheelLeft = rotate('#slide-4 .van .wheel.left', 0.6, -470, -1, 'Power0.easeNone'),
			vanBody = elastic('#slide-4 .van .body', 0.9, 5, -1),
			passenger = elastic('#slide-4 .van .passenger', 0.9, 5, -1),
			rainDrops = rainDrop('#slide-4 .rain-drop', 0.5, '-100%', '100%', -1, 0.2, 0),
			keshFadeIn = fadeInFrom('#slide-4 .kesh', 0.5, 1, 0),
			keshLightboxSlide = slideFrom('#slide-4 .lightbox .kesh', 10, '-100%', 0, 'Power0.easeNone', 0, false),
			kesh = slideTo('#slide-4 .slide-canvas .kesh', 20, '-1300%', 0, 'Power0.easeNone', -1, false),
			bikeWheels = rotate('#slide-4 .cyclist .wheel', 2.5, -360, -1, 'Power0.easeNone'),
			arrowLeft = slide('#slide-4 .arrow-left', 1, '-50%', '0', 'Elastic.easeOut'),
			arrowRight = slide('#slide-4 .arrow-right', 1, '50%', '0', 'Elastic.easeOut'),
			arrowLeftWobble = wobble('#slide-4 .arrow-left', 1, '10%', '0', 'Elastic.easeOut'),
			arrowRightWobble = wobble('#slide-4 .arrow-right', 1, '-10%', '0', 'Elastic.easeOut'),
			digitalJourney = fadeIn('#slide-4 .digital-journey', 1),
			text = scale('#slide-4 .text', 0.5);

		timeline.add([vanWheelRight,
						vanWheelLeft,
						vanBody,
						passenger,
						bikeWheels,
						rainDrops,
						keshFadeIn,
						kesh,
						keshLightboxSlide
					])
				.add(digitalJourney, 0.75)
				.add(text, 1.5)
				.add(raysFadeIn, 1)
				.add(raysRotation, 1.5)
				.add([arrowLeft, arrowRight], 2.25)
				.add([arrowRightWobble, arrowLeftWobble], 3)
				.pause();
		slides.push(timeline);
	}

	function makeSlide5() {
		var timeline = new TimelineMax(),
			arrowLeft = slide('#slide-5 .arrow-left', 1, '-100%', '0', 'Elastic.easeOut'),
			arrowRight = slide('#slide-5 .arrow-right', 1, '100%', '0', 'Elastic.easeOut'),
			arrowLeftWobble = wobble('#slide-5 .arrow-left', 1, 15, '0', 'Elastic.easeOut'),
			arrowRightWobble = wobble('#slide-5 .arrow-right', 1, -15, '0', 'Elastic.easeOut'),
			logo = scale('#slide-5 .awesome-logo', 0.75, 'Power0.easeNone', .2),
			text = fadeIn('#slide-5 .desc', 0.75),
			workForUs = scale('#slide-5 .text', 0.4);

		timeline.add(logo)
				.add(text)
				.add(workForUs)
				.add([arrowLeft, arrowRight])
				.add([arrowLeftWobble, arrowRightWobble], '-=1');
		slides.push(timeline);
	}

	function rainDrop(selector, duration, distanceX, distanceY, repeatNo, staggerTime, delayTime) {
		var	tmaxObject = TweenMax.staggerTo($(selector), duration, {
			opacity: 0,
			y: distanceY,
			x: distanceX,
			repeat: repeatNo,
			delay: delayTime
		}, staggerTime);
		return tmaxObject;
	}

	function staggerElastic(selector, duration, distance, repeatNo, staggerTime, delayTime) {
		var	tmaxObject = TweenMax.staggerTo($(selector), duration, {
			y: distance,
			repeat: repeatNo,
			yoyo: true,
			delay: delayTime
		}, staggerTime);
		return tmaxObject;
	}

	function elastic(selector, duration, distance, repeatNo) {
		var	tmaxObject = TweenMax.to($(selector), duration, {
			y: distance,
			repeat: repeatNo,
			yoyo: true
		});
		return tmaxObject;
	}

	function shoot(selector, duration, distance, easing, delayAmount, repeatNo) {
		var	tmaxObject = TweenMax.to($(selector), duration, {
			// opacity: 0;
			y: distance,
			repeat: repeatNo,
			delay: delayAmount,
			ease: easing
		});
		return tmaxObject;
	}

	function scale(selector, duration, easing, delayAmount) {
		var	tmaxObject = TweenMax.from($(selector), duration, {
			scale: 0,
			ease: easing,
			delay: delayAmount
		});
		return tmaxObject;
	}

	function bgScaleTo(selector, duration, scaleAmount, easing, repeatNo, both) {
		var	tmaxObject = TweenMax.to($(selector), duration, {
			scale: scaleAmount,
			ease: easing,
			repeat: repeatNo,
			yoyo: both
		});
		return tmaxObject;
	}

	function pulse(selector, duration, easing, repeatNo) {
		var	tmaxObject = TweenMax.from($(selector), duration, {
			opacity: 0,
			repeat: repeatNo,
			yoyo: true,
			ease: easing
		});
		return tmaxObject;
	}

	function slideTo(selector, duration, xDest, yDest, easing, repeatNo, both) {
		tmaxObject = TweenMax.to($(selector), duration, {
			x: xDest,
			y: yDest,
			yoyo: both,
			repeat: repeatNo,
			ease: easing
		});
		return tmaxObject;
	}

	function slideFrom(selector, duration, xDest, yDest, easing, repeatNo, both) {
		tmaxObject = TweenMax.from($(selector), duration, {
			x: xDest,
			y: yDest,
			yoyo: both,
			repeat: repeatNo,
			ease: easing
		});
		return tmaxObject;
	}

	function slide(selector, duration, xOrig, yOrig, easing) {
		tmaxObject = TweenMax.from($(selector), duration, {
			opacity: 0,
			x: xOrig,
			y: yOrig,
			ease: easing
		});
		return tmaxObject;
	}

	function wobble(selector, duration, xDistance, yDistance, easing) {
		tmaxObject = TweenMax.to($(selector), duration, {
			x: xDistance,
			y: yDistance,
			ease: easing,
			repeatDelay: 1,
			repeat: 3
		});
		return tmaxObject;
	}

	function rotate(selector, duration, angel, repeatNo, easing) {
		var tmaxObject = TweenMax.from($(selector), duration, {
			rotation: angel,
			repeat: repeatNo,
			ease: easing
		});
		return tmaxObject;
	}

	function fadeIn(selector, duration) {
		var tmaxObject = TweenMax.from($(selector), duration, {
			opacity: 0,
			ease: 'Power0.easeNone'
		});
		return tmaxObject;
	}

	function fadeInFrom(selector, duration, delayTime, repeatNo, delayAmount) {
		var tmaxObject = TweenMax.fromTo($(selector), duration, {opacity: 0}, {
			opacity: 1,
			delay: delayTime,
			repeat: repeatNo,
			repeatDelay: delayAmount,
			ease: 'Power0.easeNone'
		});
		return tmaxObject;
	}

	function starFall(selector, duration, xDistance, yDistance, easing) {
		var tmaxObject = TweenMax.from($(selector), duration, {opacity: 0, rotation: -180, x: xDistance, y: yDistance, ease: easing});
		return tmaxObject;
	}

	function headerAnimation(selector, duration, staggerDuration) {
		tmaxObject = TweenMax.staggerFrom($(selector), duration, {opacity: 0,  x: 0, y: '-100%', ease: Bounce.easeOut }, staggerDuration);

		return tmaxObject;
	}

	function floating(selector, duration, distance, delay, repeatNo) {
		var path;
			
		delay = typeof delay !== 'undefined' ? delay : 0;
		distance = typeof distance !== 'undefined' ? distance : 0;
		path = [{x: 0, y: 0}, {x: 0, y: 10}, {x: 0, y: 0}];
				
		tmaxObject = TweenMax.to($(selector), duration, {
			x: distance,
			repeat: repeatNo,
			yoyo: true,
			force3D: true,
			'delay': delay,
			ease: 'Power1.easeInOut',
			bezier: {values: path, curviness: 0, type: 'soft'}
		});
		return tmaxObject;
	}

	return {
		init: init,
		playSlide: playSlide,
		slides: slides
	};

})();

